import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { SuperadminModule } from '../app/superadmin/superadmin.module';
import { SchooladminModule } from '../app/schooladmin/schooladmin.module';
import { TeachersModule } from '../app/teachers/teachers.module';
import { StudentsModule } from '../app/students/students.module';
import { AuthModule } from '../app/auth/auth.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SuperadminModule,
    SchooladminModule,
    TeachersModule,
    AuthModule,
    StudentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
