import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuperadminsComponent } from '../app/superadmin/superadmins/superadmins.component';
import { SchoolsComponent } from '../app/superadmin/schools/schools.component';
import { ClassesComponent } from '../app/schooladmin/classes/classes.component';
import { SubjectsComponent } from '../app/schooladmin/subjects/subjects.component';
import { TeachersComponent } from '../app/schooladmin/teachers/teachers.component';
import { StaffComponent } from '../app/schooladmin/staff/staff.component';
import { StudentsComponent } from '../app/schooladmin/students/students.component';
import { QualifiersComponent } from '../app/teachers/qualifiers/qualifiers.component';
import { DynamicqualifiersComponent } from '../app/teachers/dynamicqualifiers/dynamicqualifiers.component';
import { QuestionsComponent } from '../app/teachers/questions/questions.component';
import { ChaptersComponent } from '../app/teachers/chapters/chapters.component';
import { SchemabuildComponent } from '../app/students/schemabuild/schemabuild.component';
import { QuestionpaperComponent } from '../app/students/questionpaper/questionpaper.component';
import { InterceptorComponent } from '../app/auth/interceptor/interceptor.component';
import { MyprofileComponent } from '../app/auth/myprofile/myprofile.component';
import { PasswordresetComponent } from '../app/auth/passwordreset/passwordreset.component';
import { SigninComponent } from '../app/auth/signin/signin.component';
const routes: Routes = [
  {path:"admin_schools",component:SchoolsComponent},
  {path:"admin_admins",component:SuperadminsComponent},
  {path:"schooladmin_classes",component:ClassesComponent},
  {path:"schooladmin_subjects",component:SubjectsComponent},
  {path:"schooladmin_teachers",component:TeachersComponent},
  {path:"schooladmin_staff",component:StaffComponent},
  {path:"schooladmin_students",component:StudentsComponent},
  {path:"teacher_qualifier",component:QualifiersComponent},
  {path:"teacher_dynamicqualifier",component:DynamicqualifiersComponent},
  {path:"teacher_questions",component:QuestionsComponent},
  {path:"teacher_chapters",component:ChaptersComponent},
  {path:"smartcompose",component:SchemabuildComponent},
  {path:"questionpaper",component:QuestionpaperComponent},
  {path:"auth_profile",component:MyprofileComponent},
  {path:"auth_pswd",component:PasswordresetComponent},
  {path:"auth_signin",component:SigninComponent},
  {path:"",component:InterceptorComponent},
];

@NgModule({  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }  
