import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchemabuildComponent } from './schemabuild.component';

describe('SchemabuildComponent', () => {
  let component: SchemabuildComponent;
  let fixture: ComponentFixture<SchemabuildComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchemabuildComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchemabuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
