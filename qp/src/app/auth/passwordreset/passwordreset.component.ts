import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
import { Router } from '@angular/router';
declare const M:any;
@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.scss']
})
export class PasswordresetComponent implements OnInit {
  resetObj:any={
    password1:"",
    password2:""
  }
  constructor(private commonService:CommonService,private router:Router) { }

  ngOnInit(): void {
  }
  async reset(){
    
    const { data, error } = await this.commonService.supabase.auth.api
    .updateUser(this.commonService.accessToken, { password :this.resetObj.password1})
    if (error==null) {
      this.commonService.isReset=false;
      this.commonService.prompt("Password has been reset Successfully \t Sign in with your new Email & password");
      this.router.navigate(["auth_signin"]);
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible'); 
    var instances = M.Collapsible.init(elems, {});
  }

}
