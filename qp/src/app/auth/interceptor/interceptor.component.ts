import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
declare const M:any;
@Component({
  selector: 'app-interceptor',
  templateUrl: './interceptor.component.html',
  styleUrls: ['./interceptor.component.scss']
})
export class InterceptorComponent implements OnInit { 

  constructor(public commonService:CommonService) { }
  signout(){
    this.commonService.supabase.auth.signOut();
  }
  ngOnInit(): void {
    
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible'); 
    var instances = M.Collapsible.init(elems, {});
  }

}
