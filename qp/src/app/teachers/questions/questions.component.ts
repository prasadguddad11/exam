import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
declare const M:any; 
declare const gridjs:any;
declare const RowSelection:any;
declare const row:any;
declare const ClassicEditor:any;
@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {
  editorC:any;
  editorQ:any;
  constructor(public commonService:CommonService) { }

  ngOnInit(): void {
    this.buildTable();
    this.getAllQualifier(this.commonService.schoolId);
    this.getAllDQualifier(this.commonService.schoolId);
    this.getAllClasses(this.commonService.schoolId)//getAllSubjects(this.classId);
    //console.log(this.classArr);
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
    ClassicEditor
                                .create( document.querySelector( '#createQueEditor' ) )
                                .then( (editor:any) => {
                                  this.editorC=editor;
                                        //console.log( this.editorC.getData() );
                                } )
                                .catch( (error:any) => {
                                        console.error( error );
                                } );
                                ClassicEditor
                                .create( document.querySelector( '#updateQueEditor' ) )
                                .then( (editor:any) => {
                                  this.editorQ=editor;
                                        //console.log( this.editorQ.getData() );
                                } )
                                .catch( (error:any) => {
                                        console.error( error );
                                } );
  }
  ngAfterViewChecked(){
    try {
      this.createObj.questionbody=this.editorC.getData();
      this.updateObj.questionbody=this.editorQ.getData();
    } catch (error) {
      
    }
    
  }
  createChanged(){
    console.log( this.editorC.getData() );
  }
  isShow:boolean=false;
  classArr:any=[];
  classId:any='';
  subjectArr:any=[];
  subjectId:any='';
  chapterArr:any=[];
  chapterId:any='';
  qualifierArr:any=[];
  dqualifierArr:any=[];
  dataArr:any=[];
  labelArr:any=[
    'id',
    'created_at',
    'questionbody',
    'questionoptions',
    'questiontype',
    'status',
    'qualifierid',
    'dynamicqualifier',
    'questionphotourl',
    ];
  gridObj:any;
  tabIndex:any=1;
  setTab(i:any){
    this.tabIndex=i;
  }
  changed(e:any){
    //console.log(e);
  }
  notice(){
    M.toast({html: 'Click on a row to select a record for updating!', classes: 'rounded'});
  }
  async getAllClasses(id:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Classes')
    .select('*')
    .eq('schoolid', id)
    if (error==null) {
      //console.log(data)
      this.classArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  async getAllSubjects(id:any){
    this.subjectArr=[];
    let { data, error } = await this.commonService.supabase
    .from('QP_Subjects')
    .select('*')
    .eq('classid', id)
    if (error==null) {
      //console.log(data)
      this.subjectArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  async getAllChapters(id:any){
    this.chapterArr=[];
    let { data, error } = await this.commonService.supabase
    .from('QP_Chapters')
    .select('*')
    .eq('subjectid', id)
    if (error==null) {
      //console.log(data)
      this.chapterArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  async getAllQualifier(id:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Qualifiers')
    .select('*')
    .eq('schoolid', id)
    if (error==null) {
      //console.log(data)
      this.qualifierArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  async getAllDQualifier(id:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_DynamicQualifiers')
    .select('*')
    .eq('schoolid', id)
    if (error==null) {
      //console.log(data)
      this.dqualifierArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  createObj:any={
    created_at:new Date(),
    status:"Active" ,
    dynamicqualifier:[""],
    qualifierid:"",
    questiontype:""
  }
  updateObj:any={


  }
  loadChapters(){
    this.getAllChapters(this.subjectId)
  }
  loadSubjects(){
    this.getAllSubjects(this.classId)
  }
  load(){
    this.getAllQuestions(this.classId)
  }
  create(){
    //this.createObj.schoolid=this.commonService.schoolId;
    this.createObj.classid=this.classId;
    this.createObj.subjectid=this.subjectId;
    this.createObj.chapterid=this.chapterId;
    this.createQuestion(this.createObj);
  }
  async createQuestion(obj:{}){
    this.commonService.prompt("Creating The Record!")
    const { data, error } = await this.commonService.supabase
    .from('QP_Questions')
    .insert([
      obj
    ])
    if (error==null) {
      this.getAllQuestions(this.chapterId)
      this.setTab(1);
      this.commonService.prompt("Record Created successfully")
    } else {
      //console.log(error);
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async getAllQuestions(chapterId:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Questions')
    .select('*')
    .eq('chapterid', this.chapterId)
    if (error==null) {
      this.dataArr=data;
      let dataarr:any[]=[];
      for (let i = 0; i < this.dataArr.length; i++) {
        let element = this.dataArr[i];
        // this.dataArr.push([
         
        //   element.id,
        //   element.created_at,
        //   element.questionbody,
        //   element.questionoptions.replace("||","\n"),
        //   element.questiontype,
        //   element.status,
        //   element.qualifierid,
        //   element.dynamicqualifier,    
        //   element.questionphotourl,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
         
        // ])
        this.dataArr[i]['dynamicqualifier']=this.getDQualifiersByIdArr(element.dynamicqualifier);
        this.dataArr[i]['qualifierid']=this.getQualifierById(element.qualifierid);
      }
      
      //console.log(dataarr)
      this.buildTable();
    } else {
      //this.commonService.prompt(JSON.stringify(error))
    }
  }
  getQualifierById(id:any){
    let val="";
    for (let i = 0; i < this.qualifierArr.length; i++) {
      
      if(id==this.qualifierArr[i].id){
        val= this.qualifierArr[i].qualifiername+"["+this.qualifierArr[i].qualifiermarks+"]"
      }
      
    }
    return val;
  }
  getIdByQualifier(name:any){
    let val='';
    for (let i = 0; i < this.qualifierArr.length; i++) {
      if(name==this.qualifierArr[i].qualifiername+"["+this.qualifierArr[i].qualifiermarks+"]"){
        val=this.qualifierArr[i].id;
      }
      
    }
    return val;
  }
  getDQualifiersByIdArr(id:any){
    let val=[];
    for (let j = 0; j < id.length; j++) {
      
    for (let i = 0; i < this.dqualifierArr.length; i++) {
      
      if(id[j]==this.dqualifierArr[i].id){
        val.push(this.dqualifierArr[i].qualifiername)
      }
      
    }
    
      
    }
    //console.log(val);
    return val.join(" || ");

  }
  getIdArrByDQualifiers(name:any){
    let val=[];
    for (let j = 0; j < name.length; j++) {
      
    for (let i = 0; i < this.dqualifierArr.length; i++) {
      //console.log(name);
      try {
        //console.log(this.dqualifierArr)
        if(name[j]==this.dqualifierArr[i].qualifiername){
          val.push(this.dqualifierArr[i].id);
        }
      } catch (error) {
        //console.log(error)
      }
      
      
    }
    
    
      
    }
    return val;
  }
  async udateQuestion(id:any){
    this.commonService.prompt("Updating the Record");
    let obj:any=this.updateObj;
    delete obj.id;
    ////console.log(this.updateObj);
    const { data, error } = await this.commonService.supabase
    .from('QP_Questions')
    .update(obj)
    .eq('id', id)
    this.updateObj.id=id;
    if (error==null) {
      this.getAllQuestions(this.chapterId);
      this.setTab(1);
      this.commonService.prompt("Record updated successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async deleteQuestion(id:any){
    const { data, error } = await this.commonService.supabase
    .from('QP_Questions')
    .delete()
    .eq('id', id)
    if (error==null) {
      this.getAllQuestions(this.chapterId);
      this.setTab(1);
      this.commonService.prompt("Record Deleted successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  buildTable(){
    (document.getElementById("wrapper") as any).innerHTML="";
    this.gridObj=new gridjs.Grid({
      sort: true,
      pagination: true,
      autoWidth:true,
      fixedHeader: true,
      height: '350px',
      width:'auto',
      search: true,
      columns: this.labelArr,
      data: this.dataArr
    })
    this.gridObj.render(document.getElementById("wrapper") as any);
    this.gridObj.forceRender(document.getElementById("wrapper") as any);
    //console.log(this.dataArr);
    this.gridObj.on('rowClick', (...args:any) => {
      this.setTab(2);
      let val:any=args[1]._cells
      this.updateObj={
      id:val[0].data,
      created_at:val[1].data,
      questionbody:val[2].data,
      questionoptions:val[3].data,
      questiontype:val[4].data,
      status:val[5].data,
      qualifierid:this.getIdByQualifier(val[6].data),
      dynamicqualifier:this.getIdArrByDQualifiers(val[7].data.split("||")),
      questionphotourl:val[8].data,
      //schoolphotourl:val[8].data
      }
      this.editorQ.setData(this.updateObj.questionbody)
      //console.log(this.updateObj,val);
      ////console.log('row: ' + JSON.stringify(args), args);
    })
    //this.gridObj.on('cellClick', (...args:any) => //console.log('cell: ' + JSON.stringify(args), args));
    let table:any=document.getElementsByClassName("gridjs-table")[0];
    table.style.width="auto";
    //table.setAttribute("style","min-width:100%")
    (document.getElementsByClassName("gridjs-container")as any)[0].style.width="100%";
  
  }
}
