import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
import { Router } from '@angular/router';
declare const M:any; 
declare const gridjs:any;
declare const RowSelection:any;
declare const row:any;
@Component({
  selector: 'app-schemabuild',
  templateUrl: './schemabuild.component.html',
  styleUrls: ['./schemabuild.component.scss']
})
export class SchemabuildComponent implements OnInit {

  constructor(public commonService:CommonService,private router:Router) { }
  getTotal(){
    let eleArr:any=document.getElementsByClassName("total");
    let count:any=0.0
    for (let i = 0; i < eleArr.length; i++) {
      count+=Number(eleArr[i].innerHTML);
      
    }
    return count;
  }
  ngOnInit(): void {
    this.buildTable();
    this.getAllClasses(this.commonService.schoolId);
    this.getAllQualifiers(this.commonService.schoolId);
    this.getAllDQualifiers(this.commonService.schoolId);
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
  }
  isShow:boolean=false;
  classArr:any=[];
  classId:any='';
  subjectArr:any=[];
  subjectId:any='';
  qualifierArr:any=[];
  dynamicQualifierArr:any=[];
  chaptArr:any=[];
  quaArr:any=[];
  dquaArr:any=[];
  schaptArr:any=[];
  squaArr:any=[];
  sdquaArr:any=[];
  schemaArr:any=[];
  dataArr:any=[];
  labelArr:any=[
    'id',
    'created_at',
    'chaptername',
    'status',
    ];
  gridObj:any;
  tabIndex:any=0;
  completionIndex:any=0;
  setTab(i:any){
    if (this.completionIndex<i) {
      this.commonService.prompt("Please fill out previuos sections completely")
    } else {
      this.tabIndex=i;
    }
  }
  notice(){
    M.toast({html: 'Click on a row to select a record for updating!', classes: 'rounded'});
  }
  async getAllClasses(id:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Classes')
    .select('*')
    .eq('schoolid', id)
    if (error==null) {
      //console.log(data)
      this.classArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      //console.log(error)
    }
  }
  async createQuestions(){
    // let arr:any=[];
    
    // for (let i = 0; i < this.chaptArr.length; i++) {
    //   for (let j = 0; j < this.qualifierArr.length; j++) {
    //     for (let k = 0; k < this.dynamicQualifierArr.length; k++) {
    //       for (let l = 0; l < 10; l++) {
    //         let queObj:any={
    //           created_at:new Date(),
    //           status:"Active" ,
    //           dynamicqualifier:[this.dynamicQualifierArr[k].id],
    //           qualifierid:this.qualifierArr[j].id,
    //           questiontype:"Discriptive",
    //           classid:this.classId,
    //           subjectid:this.subjectId,
    //           schoolid:this.commonService.schoolId,
    //           chapterid:this.chaptArr[i].id,
    //           questionbody:btoa(""+new Date()+""+i+""+j+""+k)+btoa(""+i+""+j+""+k+""+new Date()),
    //           questionoptions:""      
    //         }
    //         if (j==6) {
    //           queObj.questionoptions=btoa(""+i+""+j+""+k+""+1)+"||"+btoa(""+i+""+j+""+k+""+2)+"||"+btoa(""+i+""+j+""+k+""+3)+"||"+btoa(""+i+""+j+""+k+""+4)
           
    //         }
    //         arr.push(queObj);
    //       }
    //     }
    //   }
    // }
    // //console.log(arr)
    // const { data, error } = await this.commonService.supabase
    // .from('QP_Questions')
    // .insert(arr)
    // if (error==null) {
    //   //console.log(data)
    // } else {
    //   //console.log(error);
    // }
  }
  async getAllSubjects(id:any){
    this.subjectArr=[];
    let { data, error } = await this.commonService.supabase
    .from('QP_Subjects')
    .select('*')
    .eq('classid', id)
    if (error==null) {
      //console.log(data)
      this.subjectArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  createObj:any={
    created_at:new Date(),
    chaptername:"",
    status:"Active" 
  }
  updateObj:any={


  }
  loadSubjects(){
    this.getAllSubjects(this.classId)
  }
  load(){
    this.getAllChapters(this.subjectId)
  }
  async getAllChapters(subjectId:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Chapters')
    .select('*')
    .eq('subjectid', subjectId)
    if (error==null) {
      this.dataArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.dataArr.push([
         
          element.id,
          element.created_at,
          element.chaptername,
          element.status,
         
        ])
      }
      this.dataArr=data;
      //console.log(this.dataArr)
      this.buildTable();
    } else {
      //this.commonService.prompt(JSON.stringify(error))
    }
  }
  continue(i:any){
    if (this.completionIndex<i) {
      this.completionIndex=i;
      this.tabIndex=i;
      this.validate();
    } else {
      this.tabIndex=i;
      this.validate();
    }
  }
  async getAllQualifiers(schoolId:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Qualifiers')
    .select('*')
    .eq('schoolid', schoolId)
    if (error==null) {
      this.qualifierArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.qualifierArr.push([
         
          element.id,
          element.created_at,
          element.qualifiername,
          element.qualifierdetails,
          element.qualifiermarks,
          element.status,
         
        ])
      }
      this.qualifierArr=data;
      //console.log(this.dataArr)
      
    } else {
      //this.commonService.prompt(JSON.stringify(error))
    }
  }
  buildTable(){
    // (document.getElementById("wrapper") as any).innerHTML=""; 
    // this.gridObj=new gridjs.Grid({
    //   sort: true,
    //   pagination: true,
    //   autoWidth:true,
    //   fixedHeader: true,
    //   height: '350px',
    //   width:'auto',
    //   search: true,
    //   columns: this.labelArr,
    //   data: this.dataArr
    // })
    // this.gridObj.render(document.getElementById("wrapper") as any);
    // this.gridObj.forceRender(document.getElementById("wrapper") as any);
    // //console.log(this.dataArr);
    // this.gridObj.on('rowClick', (...args:any) => {
    //   this.setTab(2);
    //   let val:any=args[1]._cells
    //   this.updateObj={
    //   id:val[0].data,
    //   created_at:val[1].data,
    //   chaptername:val[2].data,
    //   status:val[3].data
    //   //schoolphotourl:val[8].data
    //   }
    //   //console.log(val);
    //   ////console.log('row: ' + JSON.stringify(args), args);
    // })
    // //this.gridObj.on('cellClick', (...args:any) => //console.log('cell: ' + JSON.stringify(args), args));
    // let table:any=document.getElementsByClassName("gridjs-table")[0];
    // table.style.width="auto";
    // //table.setAttribute("style","min-width:100%")
    // (document.getElementsByClassName("gridjs-container")as any)[0].style.width="100%";
  
  }
  validate(){
    let elech:any=document.getElementsByClassName("chaptname");
    let elequ:any=document.getElementsByClassName("quaname");
    let eledqu:any=document.getElementsByClassName("dquaname");
    let chaptArr:any=[];
    let quaArr:any=[];
    let dquaArr:any=[];
    
    for (let i = 0; i < elech.length; i++) {
      if (elech[i].checked) {
        chaptArr.push(this.dataArr[i]);
      } else {
        
      }
      
    }
    for (let i = 0; i < elequ.length; i++) {
      if (elequ[i].checked) {
        quaArr.push(this.qualifierArr[i]);
      } else {
        
      }
      
    }
    for (let i = 0; i < eledqu.length; i++) {
      if (eledqu[i].checked) {
        dquaArr.push(this.dynamicQualifierArr[i]);
      } else {
        
      }
      
    }
    this.chaptArr=(chaptArr);
    this.quaArr=(quaArr);
    this.dquaArr=(dquaArr);
    this.schaptArr=(chaptArr);
    this.squaArr=(quaArr);
    this.sdquaArr=(dquaArr);
    let schemarr:any=[];
    for (let i = 0; i < quaArr.length; i++) {
      schemarr.push([
        this.quaArr[i],
        this.chaptArr,
        this.dquaArr,
        0,
        0,
        this.chaptArr.map((ele:any)=>{
          return ele.id
        }),
        this.dquaArr.map((ele:any)=>{
          return ele.id
        }),
      ])
    }
    this.schemaArr=(schemarr);
  }
  chg(e:any){
    //console.log(e)
  }
  async getAllDQualifiers(schoolId:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_DynamicQualifiers')
    .select('*')
    .eq('schoolid', schoolId)
    if (error==null) {
      this.dataArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.dataArr.push([
         
          element.id,
          element.created_at,
          element.qualifiername,
          element.qualifierdetails,
          element.status,
         
        ])
      }
      this.dynamicQualifierArr=data;
      //console.log(this.dataArr)
      this.buildTable();
    } else {
      //this.commonService.prompt(JSON.stringify(error))
    }
  }
  isLoading:any=false;
  percent:any=0;
  completed:any=0;
  compose(){
    this.isLoading=true;
    this.percent=0;
    let completed:any=0;
    for (let i = 0; i < this.schemaArr.length; i++) {
      this.getFilterdLoad(i)
      
    }
  }
  async getFilterdLoad(i:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Questions')
    .select('*')
    .containedBy('dynamicqualifier', this.schemaArr[i][6])
    .in('chapterid', this.schemaArr[i][5])
    .eq('qualifierid', this.schemaArr[i][0].id)
    if (error==null) {
      let val=Number(""+this.schemaArr[i][3])+Number(""+this.schemaArr[i][4])
      let dat:any[]=(data as any)
      while (dat.length>val) {
        let num:any=Math.round((1/Math.random())*3)%(val);
        dat.splice(num,1)
      }
      //console.log(data);
      this.completed=this.completed+1;
      this.percent=Math.round((this.completed/this.schemaArr.length)*100);
      this.schemaArr[i][7]=dat;
      if(this.percent>99){
        
        this.commonService.schemaArr=this.schemaArr;
        this.isLoading=false;
        //console.log(this.commonService.schemaArr)
        this.router.navigate(["questionpaper"])
      }
    }else{
      //console.log(error)
      this.completed=this.completed+1;
      this.percent=Math.round((this.completed/this.schemaArr.length)*100);
      this.schemaArr[i][7]=[];
      if(this.percent>99){
        
        this.commonService.schemaArr=this.schemaArr
        this.isLoading=false;
      }
    }
  }
}
