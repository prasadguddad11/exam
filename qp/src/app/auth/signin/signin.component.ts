import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
declare const M:any;
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  signinObj:any={
    email:"",
    password:""
  };
  signupObj:any={
    email:"",
    password1:"",
    password2:""
  };
  resetObj:any={
    email:""
  }
  constructor(private commonService:CommonService) { }
  
  ngOnInit(): void { 
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible'); 
    var instances = M.Collapsible.init(elems, {});
  }
  async signin(){
    const { user, session, error } = await this.commonService.supabase.auth.signIn({
      email: this.signinObj.email,
      password: this.signinObj.password
    })
    //console.log(user, session, error)
    if (error==null) {
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async signUp(){
    const { user, session, error } = await this.commonService.supabase.auth.signUp({
      email: this.signupObj.email,
      password:this.signupObj.password1,
    })
    if (error==null) {
      const resp = await this.commonService.supabase.auth.signOut()
                    this.commonService.prompt("User Successfully Registered");
                    this.commonService.prompt("Verification mail has been sent!!");
                    this.commonService.prompt("Click  the link in your mail to confirm and verify!!");
      
                    
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async resetmail(){
    const { data, error } = await this.commonService.supabase.auth.api
                              .resetPasswordForEmail(this.resetObj.email)
    if (error==null) {
      this.commonService.prompt("Password reset mail has been sent!!");
      this.commonService.prompt("Click  the link in your mail to reset your password!!");
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
}
