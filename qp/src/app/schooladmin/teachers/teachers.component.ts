import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
declare const M:any; 
declare const gridjs:any;
declare const RowSelection:any;
declare const row:any;
@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss'] 
})
export class TeachersComponent implements OnInit {
  constructor(public commonService:CommonService) { }

  ngOnInit(): void {
    this.getAllTeachers();
  }
  createObj:any={
    created_at:new Date(),
    firstname:"",
    middlename:"",
    lastname:"",
    email:"",
    phone:"",
    address:"",
    status:"Active",
    
}
updateObj:any={


}
  create(){
    this.createObj.roleid=4
    this.createObj.schoolid=this.commonService.schoolId;
    this.createTeacher(this.createObj);
  }
  async getAllTeachers(){
    let { data, error } = await this.commonService.supabase
    .from('QP_Teachers')
    .select('*')
    .eq('roleid',4)
    .eq('schoolid',this.commonService.schoolId)
    if (error==null) {
      this.dataArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.dataArr.push([
         
          element.id,
          element.created_at,
          element.firstname,
          element.middlename,
          element.lastname,
          element.email,
          element.phone,
          element.address,
          element.status
        ])
      }
      this.dataArr=data;
      //console.log(this.dataArr)
      this.buildTable();
    } else {
      //this.commonService.prompt(JSON.stringify(error))
    }
  }
  async udateTeacher(id:any){
    this.commonService.prompt("Updating the Record");
    let obj:any=this.updateObj;
    delete obj.id
    //console.log(this.updateObj);
    const { data, error } = await this.commonService.supabase
    .from('QP_Teachers')
    .update(obj)
    .eq('id', id)
    if (error==null) {
      this.getAllTeachers();
      this.setTab(1)
      this.commonService.prompt("Record updated successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async createTeacher(obj:{}){
    //console.log(obj);
    const { data, error } = await this.commonService.supabase
    .from('QP_Teachers')
    .insert([
      obj
    ])
    if (error==null) {
      this.getAllTeachers();
      this.setTab(1)
      this.commonService.prompt("Record Created successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async deleteTeacher(id:any){
    const { data, error } = await this.commonService.supabase
    .from('QP_Teachers')
    .delete()
    .eq('id', id)
    if (error==null) {
      this.getAllTeachers();
      this.setTab(1)
      this.commonService.prompt("Record Deleted successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
    this.buildTable();
    //this.getAllTeachers();
  }
  dataArr:any=[];
  labelArr:any=[
  'id',
  'created_at',
  'firstname',
  'middlename',
  'lastname',
  'email',
  'phone',
  'address',
  'status'];
  gridObj:any;
buildTable(){
  (document.getElementById("wrapper") as any).innerHTML="";
  this.gridObj=new gridjs.Grid({
    sort: true,
    pagination: true,
    autoWidth:true,
    fixedHeader: true,
    height: '350px',
    width:'auto',
    search: true,
    columns: this.labelArr,
    data: this.dataArr
  })
  this.gridObj.render(document.getElementById("wrapper") as any);
  this.gridObj.forceRender(document.getElementById("wrapper") as any);
  //console.log(this.dataArr);
  this.gridObj.on('rowClick', (...args:any) => {
    this.setTab(2);
    let val:any=args[1]._cells
    this.updateObj={
    id:val[0].data,
    created_at:val[1].data,
    firstname:val[2].data,
    middlename:val[3].data,
    lastname:val[4].data,
    email:val[5].data,
    phone:val[6].data,
    address:val[7].data,
    status:val[8].data
    }
    //console.log(val);
    ////console.log('row: ' + JSON.stringify(args), args);
  })
  //this.gridObj.on('cellClick', (...args:any) => //console.log('cell: ' + JSON.stringify(args), args));
  let table:any=document.getElementsByClassName("gridjs-table")[0];
  table.style.width="auto";
  //table.setAttribute("style","min-width:100%")
  (document.getElementsByClassName("gridjs-container")as any)[0].style.width="100%";

}

tabIndex:any=1;
  setTab(i:any){
    this.tabIndex=i;
  }
  notice(){
    M.toast({html: 'Click on a row to select a record for updating!', classes: 'rounded'});
  }

}
