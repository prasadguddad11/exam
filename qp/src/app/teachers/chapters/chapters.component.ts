import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
declare const M:any; 
declare const gridjs:any;
declare const RowSelection:any;
declare const row:any;
@Component({ 
  selector: 'app-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.scss']
})
export class ChaptersComponent implements OnInit {

  constructor(public commonService:CommonService) { }

  ngOnInit(): void {
    this.buildTable();
    this.getAllClasses(this.commonService.schoolId)//getAllSubjects(this.classId);
    //console.log(this.classArr);
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
  }
  isShow:boolean=false;
  classArr:any=[];
  classId:any='';
  subjectArr:any=[];
  subjectId:any='';
  dataArr:any=[];
  labelArr:any=[
    'id',
    'created_at',
    'chaptername',
    'status',
    ];
  gridObj:any;
  tabIndex:any=1;
  setTab(i:any){
    this.tabIndex=i;
  }
  notice(){
    M.toast({html: 'Click on a row to select a record for updating!', classes: 'rounded'});
  }
  async getAllClasses(id:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Classes')
    .select('*')
    .eq('schoolid', id)
    if (error==null) {
      //console.log(data)
      this.classArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  async getAllSubjects(id:any){
    this.subjectArr=[];
    let { data, error } = await this.commonService.supabase
    .from('QP_Subjects')
    .select('*')
    .eq('classid', id)
    if (error==null) {
      //console.log(data)
      this.subjectArr= data;  
         
    } else {
      //this.commonService.prompt(JSON.stringify(error))
      //return data;
      ////console.log(error)
    }
  }
  createObj:any={
    created_at:new Date(),
    chaptername:"",
    status:"Active" 
  }
  updateObj:any={


  }
  loadSubjects(){
    this.getAllSubjects(this.classId)
  }
  load(){
    this.getAllChapters(this.subjectId)
  }
  create(){
    this.createObj.schoolid=this.commonService.schoolId;
    this.createObj.classid=this.classId;
    this.createObj.subjectid=this.subjectId
    this.createChapter(this.createObj);
  }
  async createChapter(obj:{}){
    this.commonService.prompt("Creating The Record!")
    const { data, error } = await this.commonService.supabase
    .from('QP_Chapters')
    .insert([
      obj
    ])
    if (error==null) {
      this.getAllChapters(this.subjectId)
      this.setTab(1);
      this.commonService.prompt("Record Created successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async getAllChapters(subjectId:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Chapters')
    .select('*')
    .eq('subjectid', subjectId)
    if (error==null) {
      this.dataArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.dataArr.push([
         
          element.id,
          element.created_at,
          element.chaptername,
          element.status,
         
        ])
      }
      this.dataArr=data;
      //console.log(this.dataArr)
      this.buildTable();
    } else {
      //this.commonService.prompt(JSON.stringify(error))
    }
  }
  async udateChapter(id:any){
    this.commonService.prompt("Updating the Record");
    let obj:any=this.updateObj;
    delete obj.id;
    ////console.log(this.updateObj);
    const { data, error } = await this.commonService.supabase
    .from('QP_Chapters')
    .update(obj)
    .eq('id', id)
    this.updateObj.id=id;
    if (error==null) {
      this.getAllChapters(this.subjectId);
      this.setTab(1);
      this.commonService.prompt("Record updated successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async deleteChapter(id:any){
    const { data, error } = await this.commonService.supabase
    .from('QP_Chapters')
    .delete()
    .eq('id', id)
    if (error==null) {
      this.getAllChapters(this.subjectId);
      this.setTab(1);
      this.commonService.prompt("Record Deleted successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  buildTable(){
    (document.getElementById("wrapper") as any).innerHTML="";
    this.gridObj=new gridjs.Grid({
      sort: true,
      pagination: true,
      autoWidth:true,
      fixedHeader: true,
      height: '350px',
      width:'auto',
      search: true,
      columns: this.labelArr,
      data: this.dataArr
    })
    this.gridObj.render(document.getElementById("wrapper") as any);
    this.gridObj.forceRender(document.getElementById("wrapper") as any);
    //console.log(this.dataArr);
    this.gridObj.on('rowClick', (...args:any) => {
      this.setTab(2);
      let val:any=args[1]._cells
      this.updateObj={
      id:val[0].data,
      created_at:val[1].data,
      chaptername:val[2].data,
      status:val[3].data
      //schoolphotourl:val[8].data
      }
      //console.log(val);
      ////console.log('row: ' + JSON.stringify(args), args);
    })
    //this.gridObj.on('cellClick', (...args:any) => //console.log('cell: ' + JSON.stringify(args), args));
    let table:any=document.getElementsByClassName("gridjs-table")[0];
    table.style.width="auto";
    //table.setAttribute("style","min-width:100%")
    (document.getElementsByClassName("gridjs-container")as any)[0].style.width="100%";
  
  }
}

