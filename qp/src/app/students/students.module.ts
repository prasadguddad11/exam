import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SchemabuildComponent } from './schemabuild/schemabuild.component';
import { QuestionpaperComponent } from './questionpaper/questionpaper.component';



@NgModule({
  declarations: [
    SchemabuildComponent,
    QuestionpaperComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class StudentsModule { }
