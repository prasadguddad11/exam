import { Component, OnInit } from '@angular/core';
import { CommonService } from "./common.service";
import { Router } from '@angular/router';
declare const M:any; 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(public commonService:CommonService,private router:Router) { }
  title = 'GYAN - 360';
  getRoute(){
    return this.route[this.router.url.replace('/','')];
  }
  getIcon(){
    return this.icon[this.router.url.replace('/','')];
  }
  user:any;
  route:any={
    admin_admins:'SuperAdmins',
    admin_schools:'Schools',
    schooladmin_classes:"Classes",
    schooladmin_subjects:"Subjects",
    schooladmin_teachers:"Teachers",
    schooladmin_staff:"Staff",
    schooladmin_students:"Students",
    teacher_chapters:"Chapters",
    teacher_qualifier:"StaticQualifier",
    teacher_dynamicqualifier:"DynamicQualifier", 
    teacher_questions:"Questions",
    smartcompose:"SmartCompose",
    questionpaper:"QuestionPaper",
    auth_signin:"Authentication",
    auth_profile:"Profile"
  };
  icon:any={
    admin_admins:'admin_panel_settings',
    admin_schools:'maps_home_work',
    schooladmin_classes:"groups",
    schooladmin_subjects:"folder_special",
    schooladmin_teachers:"cast_for_education",
    schooladmin_staff:"engineering",
    schooladmin_students:"local_library",
    teacher_chapters:"auto_stories", 
    teacher_qualifier:"local_police",
    teacher_dynamicqualifier:"filter_alt",
    teacher_questions:"quiz",
    smartcompose:"widgets",
    questionpaper:"history_edu",
    auth_signin:"privacy_tip",
    auth_profile:"person"
  };

  ngOnInit(){
    setInterval(()=>{
      // if(this.utilityService.authModal==null){
      //   this.router.navigate(['signin']);
      // }
      
    },200)
    //this.user=this.utilityService.authModal.clerkEmail.split('@')[0];
  }
  elems:any;
  instances:any;
  ngAfterViewInit(){
    this.elems = document.querySelectorAll('.tooltipped');
    let bd:any|HTMLBodyElement=document.getElementsByTagName("body")[0]
    if(bd.offsetWidth>500){
    this.instances = M.Tooltip.init(this.elems,{enterDelay:0});
    }
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {});
  }
  isExpanded:boolean=false;
  toggle(){
    this.isExpanded=!this.isExpanded;
  }
  move(path:any){
    try {
      this.router.navigate([path])
      let bd:any|HTMLBodyElement=document.getElementsByTagName("body")[0]
      if(bd.offsetWidth<500){
        this.toggle();
      }
      
    } catch (error) {
      
    }
    
  }
  isSelected(path:any):boolean{
    let val:boolean=false;
    if(location.href.includes(path)){
      val=true;
    }
    return val;
  }
  expand(){
    alert("")
  }
  
}

 

