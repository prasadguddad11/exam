import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { InterceptorComponent } from './interceptor/interceptor.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SigninComponent,
    MyprofileComponent,
    PasswordresetComponent,
    InterceptorComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class AuthModule { }
