import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
declare const M:any; 
declare const gridjs:any;
declare const RowSelection:any;
declare const row:any;
@Component({
  selector: 'app-qualifiers',
  templateUrl: './qualifiers.component.html',
  styleUrls: ['./qualifiers.component.scss']
})
export class QualifiersComponent implements OnInit {

  constructor(public commonService:CommonService) { }

  ngOnInit(): void {
    this.buildTable();
    this.getAllQualifiers(this.commonService.schoolId)
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible');  
    var instances = M.Collapsible.init(elems);
  }
  isShow:boolean=false;
  
  dataArr:any=[];
  labelArr:any=[
    'id',
    'created_at',
    'qualifiername',
    'qualifierdetails',
    'qualifiermarks',
    'status',
    ];
  gridObj:any;
  tabIndex:any=1;
  setTab(i:any){
    this.tabIndex=i;
  }
  notice(){
    M.toast({html: 'Click on a row to select a record for updating!', classes: 'rounded'});
  }
  
  
  createObj:any={
    created_at:new Date(),
    qualifiername:"",
    qualifierdetails:"",
    qualifiermarks:"",
    status:"Active" 
  }
  updateObj:any={


  }
  
  create(){
    this.createObj.schoolid=this.commonService.schoolId;
    
    this.createQualifier(this.createObj);
  }
  async createQualifier(obj:{}){
    this.commonService.prompt("Creating The Record!")
    const { data, error } = await this.commonService.supabase
    .from('QP_Qualifiers')
    .insert([
      obj
    ])
    if (error==null) {
      this.getAllQualifiers(this.commonService.schoolId)
      this.setTab(1);
      this.commonService.prompt("Record Created successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async getAllQualifiers(schoolId:any){
    let { data, error } = await this.commonService.supabase
    .from('QP_Qualifiers')
    .select('*')
    .eq('schoolid', schoolId)
    if (error==null) {
      this.dataArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.dataArr.push([
         
          element.id,
          element.created_at,
          element.qualifiername,
          element.qualifierdetails,
          element.qualifiermarks,
          element.status,
         
        ])
      }
      this.dataArr=data;
      //console.log(this.dataArr)
      this.buildTable();
    } else {
      //this.commonService.prompt(JSON.stringify(error))
    }
  }
  async udateQualifier(id:any){
    this.commonService.prompt("Updating the Record");
    let obj:any=this.updateObj;
    delete obj.id;
    ////console.log(this.updateObj);
    const { data, error } = await this.commonService.supabase
    .from('QP_Qualifiers')
    .update(obj)
    .eq('id', id)
    this.updateObj.id=id;
    if (error==null) {
      this.getAllQualifiers(this.commonService.schoolId);
      this.setTab(1);
      this.commonService.prompt("Record updated successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async deleteQualifier(id:any){
    const { data, error } = await this.commonService.supabase
    .from('QP_Chapters')
    .delete()
    .eq('id', id)
    if (error==null) {
      this.getAllQualifiers(this.commonService.schoolId);
      this.setTab(1);
      this.commonService.prompt("Record Deleted successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  buildTable(){
    (document.getElementById("wrapper") as any).innerHTML="";
    this.gridObj=new gridjs.Grid({
      sort: true,
      pagination: true,
      autoWidth:true,
      fixedHeader: true,
      height: '350px',
      width:'auto',
      search: true,
      columns: this.labelArr,
      data: this.dataArr
    })
    this.gridObj.render(document.getElementById("wrapper") as any);
    this.gridObj.forceRender(document.getElementById("wrapper") as any);
    //console.log(this.dataArr);
    this.gridObj.on('rowClick', (...args:any) => {
      this.setTab(2);
      let val:any=args[1]._cells
      this.updateObj={
      id:val[0].data,
      created_at:val[1].data,
      qualifiername:val[2].data,
      qualifierdetails:val[3].data,
      qualifiermarks:val[4].data,
      status:val[5].data
      }
      //console.log(this.updateObj);
      ////console.log('row: ' + JSON.stringify(args), args);
    })
    //this.gridObj.on('cellClick', (...args:any) => //console.log('cell: ' + JSON.stringify(args), args));
    let table:any=document.getElementsByClassName("gridjs-table")[0];
    table.style.width="auto";
    //table.setAttribute("style","min-width:100%")
    (document.getElementsByClassName("gridjs-container")as any)[0].style.width="100%";
  
  }
}
