import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SchoolsComponent } from './schools/schools.component';
import { SuperadminsComponent } from './superadmins/superadmins.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SchoolsComponent,
    SuperadminsComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule
    
  ],
  exports:[
    SchoolsComponent,
    SuperadminsComponent
  ]
})
export class SuperadminModule { }
