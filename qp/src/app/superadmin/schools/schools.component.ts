import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
declare const M:any; 
declare const gridjs:any;
declare const RowSelection:any;
declare const row:any;
 
@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.scss']
})
export class SchoolsComponent implements OnInit {
  dataArr:any=[];
  labelArr:any=[
    'id',
    'created_at',
    'schoolname',
    'schooltag',
    'schoolemail',
    'schoolphone',
    'schooladdress',
    'status'
    
  ];
  createObj:any={
          created_at:new Date(),
          schoolname:"",
          schooltag:"",
          schoolemail:"",
          schoolphone:"",
          schooladdress:"",
          status:"Active"
         
  }
  updateObj:any={
    
    
  }
  constructor(private commonService:CommonService) {
    this.getAllSchools();
  }

  ngOnInit(): void {
    

  }
  create(){
    this.createSchool(this.createObj);
  }
  async getAllSchools(){
    let { data, error } = await this.commonService.supabase
    .from('QP_Schools')
    .select('*')
    //.eq('roleid', 1)
    if (error==null) {
      this.dataArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.dataArr.push([
         
          element.id,
          element.created_at,
          element.schoolname,
          element.schooltag,
          element.schoolemail,
          element.schoolphone,
          element.schooladdress,
          element.status
        ])
      }
      this.dataArr=data;
      //console.log(this.dataArr)
      this.buildTable();
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async udateSchool(id:any){
    this.commonService.prompt("Updating the Record");
    let obj:any=this.updateObj;
    delete obj.id
    ////console.log(this.updateObj);
    const { data, error } = await this.commonService.supabase
    .from('QP_Schools')
    .update(obj)
    .eq('id', id)
    this.updateObj.id=id;
    if (error==null) {
      this.getAllSchools();
      this.commonService.prompt("Record updated successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async createSchool(obj:{}){
    const { data, error } = await this.commonService.supabase
    .from('QP_Schools')
    .insert([
      obj
    ])
    if (error==null) {
      this.getAllSchools();
      this.commonService.prompt("Record Created successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  async deleteSchool(id:any){
    const { data, error } = await this.commonService.supabase
    .from('QP_Schools')
    .delete()
    .eq('id', id)
    if (error==null) {
      this.getAllSchools();
      this.commonService.prompt("Record Deleted successfully")
    } else {
      this.commonService.prompt(JSON.stringify(error))
    }
  }
  ngAfterViewInit(){
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
    this.buildTable();
  }
 
  gridObj:any;
buildTable(){
  (document.getElementById("wrapper") as any).innerHTML="";
  this.gridObj=new gridjs.Grid({
    sort: true,
    pagination: true,
    autoWidth:true,
    fixedHeader: true,
    height: '350px',
    width:'auto',
    search: true,
    columns: this.labelArr,
    data: this.dataArr
  })
  this.gridObj.render(document.getElementById("wrapper") as any);
  this.gridObj.forceRender(document.getElementById("wrapper") as any);
  //console.log(this.dataArr);
  this.gridObj.on('rowClick', (...args:any) => {
    this.setTab(2);
    let val:any=args[1]._cells
    this.updateObj={
    id:val[0].data,
    created_at:val[1].data,
    schoolname:val[2].data,
    schooltag:val[3].data,
    schoolemail:val[4].data,
    schoolphone:val[5].data,
    schooladdress:val[6].data,
    status:val[7].data,
    //schoolphotourl:val[8].data
    }
    //console.log(val);
    ////console.log('row: ' + JSON.stringify(args), args);
  })
  //this.gridObj.on('cellClick', (...args:any) => //console.log('cell: ' + JSON.stringify(args), args));
  let table:any=document.getElementsByClassName("gridjs-table")[0];
  table.style.width="auto";
  //table.setAttribute("style","min-width:100%")
  (document.getElementsByClassName("gridjs-container")as any)[0].style.width="100%";
}

tabIndex:any=1;
  setTab(i:any){
    this.tabIndex=i;
  }
  notice(){
    M.toast({html: 'Click on a row to select a record for updating!', classes: 'rounded'});
  }

}
