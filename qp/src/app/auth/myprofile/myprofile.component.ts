import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.scss']
})
export class MyprofileComponent implements OnInit {

  constructor(public commonService:CommonService) { }
  snot(){
    this.commonService.supabase.auth.signOut();
  }
  ngOnInit(): void {
    //console.log(this.commonService.userDetails)
  }

}
