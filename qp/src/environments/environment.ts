// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  _projectUrl:'https://nsjhbghappbwukusbiez.supabase.co',
  _publicKey:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im5zamhiZ2hhcHBid3VrdXNiaWV6Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2NDkzMDk0ODQsImV4cCI6MTk2NDg4NTQ4NH0.bGkSBkyICU6LBZPGyA2opSMXE-WfMPfVOEmjGKq4R-s'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
