import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeachersComponent } from './teachers/teachers.component';
import { StaffComponent } from './staff/staff.component';
import { StudentsComponent } from './students/students.component';
import { ClassesComponent } from './classes/classes.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TeachersComponent,
    StaffComponent,
    StudentsComponent,
    ClassesComponent,
    SubjectsComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class SchooladminModule { }
