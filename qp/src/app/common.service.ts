import { Injectable } from '@angular/core';
import { createClient } from '@supabase/supabase-js';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
declare const M:any; 
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  user:any={
  }
  accessToken:any="";
  refreshToken:any="";
  userDetails:any={
  };
  schemaArr:any=[];
  event:any="";
  isReset:boolean=false;
  async getRoleAndName(email:any){
   
    let { data, error } = await this.supabase
    .from('QP_Admins')
    .select("*")
    .eq('email', email)
    if ((data as any).length>0.0 && error==null) {
      this.userDetails=(data as any)[0];
      this.user.roleId=1;
      this.user.roleName="Admin";
    } else {
            let { data, error } = await this.supabase
            .from('QP_Schools')
            .select("*")
            .eq('schoolemail', email)
            if ((data as any).length>0.0 && error==null) {
              this.userDetails=(data as any)[0];
              this.user.roleId=2;
              this.user.roleName="School";
              this.schoolId=this.userDetails.id;
              if (this.isReset==false) {
                this.router.navigate(["auth_profile"])
              } else {
                
              }
            } else {
                    let { data, error } = await this.supabase
                    .from('QP_Staff')
                    .select("*")
                    .eq('email', email)
                    if ((data as any).length>0.0 && error==null) {
                      this.userDetails=(data as any)[0];
                      this.user.roleId=3;
                      this.user.roleName="Staff";
                    } else {
                            let { data, error } = await this.supabase
                            .from('QP_Teachers')
                            .select("*")
                            .eq('email', email)
                            if ((data as any).length>0.0 && error==null) {
                              this.userDetails=(data as any)[0];
                              this.user.roleId=4;
                              this.user.roleName="Teacher";
                            } else {
                                    let { data, error } = await this.supabase
                                    .from('QP_Students')
                                    .select("*")
                                    .eq('email', email)
                                    if ((data as any).length>0.0 && error==null) {
                                      this.userDetails=(data as any)[0];
                                      this.user.roleId=5;
                                      this.user.roleName="Student";
                                    } else {
                                      this.user.roleId=0;
                                      this.user.roleName="Unknown"; 
                                    }
                            }
                    }
            }
    }
    if (this.userDetails.schoolid!=null) {
      if (this.user.roleId!=2) {
        this.schoolId=this.userDetails.schoolid;
        //console.log(this.userDetails)
      } else {
        this.schoolId=this.userDetails.id;
        //console.log(this.userDetails)
      }
      
    }
  }
  //http://localhost:4200/#access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhdXRoZW50aWNhdGVkIiwiZXhwIjoxNjUxMTUzNjA1LCJzdWIiOiJjMGM1ZGExMC1hMzY1LTRhYmYtODRkMy01OTE3M2EzZDFhZWMiLCJlbWFpbCI6InByYXNhZGd1ZGRhZDExQGdtYWlsLmNvbSIsInBob25lIjoiIiwiYXBwX21ldGFkYXRhIjp7InByb3ZpZGVyIjoiZW1haWwiLCJwcm92aWRlcnMiOlsiZW1haWwiXX0sInVzZXJfbWV0YWRhdGEiOnt9LCJyb2xlIjoiYXV0aGVudGljYXRlZCJ9.f0jT7iKQdp4LIrRaYNURfI6bO59UiYni6pQgXjtbIcs&expires_in=3600&refresh_token=5Hw46Us15z_OTT5SMxUGeA&token_type=bearer&type=recovery
  constructor(private router:Router) { 
    //console.log(location.href.split("&"))
    let token:any=(location.href.split("&"));
    token=token[token.length-1]
    
    if (token=="type=recovery") {
      this.isReset=true;
      this.supabase.auth.signOut();
    } else {
      
    }
    setInterval(()=>{
      let arr1:any[]=["/auth_signin","/auth_pswd","/"]
      let currUrl=this.router.url;
      //console.log(this.schoolId);
      if (this.isReset==true) {
        this.router.navigate(["auth_pswd"]);
      }
      else if (currUrl!="/auth_signin"&&this.user.email==undefined) {
        this.router.navigate(["auth_signin"]);
      }
      else if (this.user.email!=undefined&&(this.user.roleId==0||this.user.isEmailVerified==false)) {
        this.router.navigate([""]);
      } else if (this.user.email!=undefined &&arr1.includes(currUrl)) {
        //console.log(this.isReset)
        this.router.navigate(["auth_profile"]);
      }
    },200)
    this.supabase.auth.onAuthStateChange((event:any, session:any) => {
      //console.log(event, session);
      this.event=event;
      if(event == 'SIGNED_IN'){
        this.user.email=session.user.email;
        this.user.isEmailVerified=session.user.email_confirmed_at.length<10?false:true;
        this.accessToken=session.access_token;
        this.refreshToken=session.refreshToken;
        if(this.isReset!=true){
         
          setTimeout(()=>{
            this.getRoleAndName(session.user.email);
            
          },10)
        
        }else{
          this.supabase.auth.signOut();
        }
      }
      if(event == 'SIGNED_OUT'){
        this.user={};
        this.userDetails={};
        if (this.isReset==false) {
          //this.router.navigate(["auth_signin"]) 
        }
      }
      if(event == 'PASSWORD_RECOVERY'){
        this.user.email=session.user.email;
        this.user.isEmailVerified=session.user.email_confirmed_at.length<10?false:true;
        this.accessToken=session.access_token;
        this.refreshToken=session.refreshToken;
        // setTimeout(() => {
        //   this.isReset=true;
        //   this.supabase.auth.signOut();
        //   this.router.navigate(["auth_pswd"]);
        // }, 200);
      }
      //console.log(event, session);
    })
    this.getAllSchools()
  }
  supabase = createClient(environment._projectUrl, environment._publicKey);
  prompt(msg:any){
    M.toast({html: msg, classes: 'rounded'});
  }
  schoolId:any='';
  schoolArr:any=[];
  async getAllSchools(){
    let { data, error } = await this.supabase
    .from('QP_Schools')
    .select('*')
    //.eq('roleid', 1)
    if (error==null) {
      this.schoolArr=[];
      let dataarr:any[]=[];
      for (let i = 0; i < dataarr.length; i++) {
        let element = dataarr[i];
        this.schoolArr.push([
         
          element.id,
          element.created_at,
          element.schoolname,
          element.schooltag,
          element.schoolemail,
          element.schoolphone,
          element.schooladdress,
          element.status
        ])
      }
      this.schoolArr=data;
      //console.log(this.schoolArr)
      
    } else {
      //this.prompt(JSON.stringify(error))
    }
  }
  
}
