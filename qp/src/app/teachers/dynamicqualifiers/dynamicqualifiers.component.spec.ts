import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicqualifiersComponent } from './dynamicqualifiers.component';

describe('DynamicqualifiersComponent', () => {
  let component: DynamicqualifiersComponent;
  let fixture: ComponentFixture<DynamicqualifiersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicqualifiersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicqualifiersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
