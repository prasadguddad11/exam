import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionsComponent } from './questions/questions.component';
import { QualifiersComponent } from './qualifiers/qualifiers.component';
import { ComposeComponent } from './compose/compose.component';
import { ChaptersComponent } from './chapters/chapters.component';
import { FormsModule } from '@angular/forms';
import { FiltersComponent } from './filters/filters.component';
import { DynamicqualifiersComponent } from './dynamicqualifiers/dynamicqualifiers.component';


@NgModule({
  declarations: [
    QuestionsComponent, 
    QualifiersComponent,
    ComposeComponent,
    ChaptersComponent,
    FiltersComponent,
    DynamicqualifiersComponent 
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class TeachersModule { }
