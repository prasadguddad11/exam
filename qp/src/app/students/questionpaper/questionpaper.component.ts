import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common.service";
import { Router } from '@angular/router';
declare const M:any; 
declare const gridjs:any;
declare const RowSelection:any;
declare const row:any;
@Component({
  selector: 'app-questionpaper',
  templateUrl: './questionpaper.component.html',
  styleUrls: ['./questionpaper.component.scss']
})
export class QuestionpaperComponent implements OnInit {
  schemaArr:any=[];
  constructor(public commonService:CommonService,private router:Router) { 
    this.schemaArr=commonService.schemaArr;
  }

  ngOnInit(): void {
  }

}
